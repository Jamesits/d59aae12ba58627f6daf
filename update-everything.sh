#!/usr/bin/env bash

update() {
    case $1 in
    
    # apt-get
        apt)
            apt update && apt full-upgrade -y
        ;;
    
    # brew.sh
        brew)
            brew update && brew outdated && brew upgrade --all && brew cleanup
    
    # Docker
        docker)
            docker images | awk '{print $1}' | xargs -L1 docker pull
        ;;
    
    # node.js
        npm)
            npm update -g
        ;;
        
    # Pacman
        pacman)
            pacman -Syy
            pacman -Syu --noconfirm
        ;;
        
    # Python
        pip2)
            pip2 install --upgrade pip
            pip2 freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip2 install -U
        ;;
        pip3)
            pip3 install --upgrade pip
            pip3 freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip3 install -U
        ;;
    
    # Ruby
        gem)
            gem update `gem outdated | cut -d ' ' -f 1`
        ;;
        
    # yaourt
        yaourt)
            yaourt -Syy
            yaourt -Syu --noconfirm
        ;;
    
    esac
}
    